"""
GReNaDIne: Gene Regulatory Network Data-driven Inference

This package allows to infer Gene Regulatory Networks through several
Data-driven methods. Pre-processing and evaluation methods are also included.
"""
__author__ = "Sergio Peignier and Pauline Schmitt"
__copyright__ = "Copyright 2019, The GReNaDIne Project"
__license__ = "GPL"
__version__ = "0.1.15"
__maintainer__ = "Sergio Peignier"
__email__ = "sergio.peignier@insa-lyon.fr"
__status__ = "pre-alpha"
