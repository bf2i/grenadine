.. grenadine documentation master file, created by
   sphinx-quickstart on Mon Jul  1 09:51:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to grenadine's documentation!
=====================================

.. image:: _static/logo_grenadine_white.png
   :width: 150
   :align: center

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   grenadine
   grenadine.Preprocessing
   grenadine.Inference
   grenadine.Evaluation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
