grenadine.Evaluation package
============================

Submodules
----------

grenadine.Evaluation.evaluation module
--------------------------------------

.. automodule:: grenadine.Evaluation.evaluation
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: grenadine.Evaluation
   :members:
   :undoc-members:
   :show-inheritance:
